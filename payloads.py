import os.path
import platform
import subprocess
from dew import payloads


class CMakeBuildPayload(payloads.Payload):

    def __init__(self, props, formatter, install_info):
        super(CMakeBuildPayload, self).__init__(props)
        self._source_dir = formatter.format(props.get('source_dir', ''))
        self._target_name = formatter.format(props['target_name'])

        stage = self._vcvars_path = install_info.stage_controller.stage
        self._stage_path = stage.path

        self._generator = stage.options['cmake-generator']
        self._vcvars_path = stage.options.get('build-vcvars-path')
        self._vcvars_arch = stage.options.get('build-vcvars-arch')

        is_vs_generator = self._generator.startswith('Visual Studio')

        out_dir_prefix = '.'
        library_prefix = 'lib'
        shared_library_extension = 'so'
        static_library_extension = 'a'
        if platform.system() == 'Darwin':
            shared_library_extension = 'dylib'
        if is_vs_generator:
            library_prefix = ''
            out_dir_prefix = 'Debug'
            shared_library_extension = 'lib'
            static_library_extension = 'lib'

        self._out_dir_prefix = out_dir_prefix
        self._library_prefix = library_prefix
        self._shared_library_extension = shared_library_extension
        self._static_library_extension = static_library_extension

    def execute(self, path_service, install_info, stage_files):
        stage = install_info.stage_controller.stage
        # TODO: Better env handling.
        # env = {
        #     'PATH': os.path.join(stage.path, 'bin')
        # }
        cwd = os.path.join(install_info.cellar_path, self._source_dir)
        subprocess.check_call(['cmake', '-G', self._generator], cwd=cwd)
        self._build(cwd)

    def _build(self, cwd):
        if self._generator == 'Unix Makefiles':
            self._build_make(cwd)
        elif self._generator.startswith('Visual Studio'):
            self._build_msvc(cwd)
        else:
            raise Exception('Oops, the cmake plugin doesn\'t know how to build with your generator, plus this is a shitty error message')

    def _build_msvc(self, cwd):
        # Lets get the environment variables we need from vcvarsall! What a piece of shit!
        root = os.path.join(self._stage_path, 'share', 'dew', 'cmake-builder')
        echo_script_path = os.path.join(root, 'echo_vc_vars.bat')
        run_msbuild_script_path = os.path.join(root, 'run_msbuild.bat')
        echo_output = subprocess.check_output([echo_script_path, self._vcvars_path, self._vcvars_arch],
                                              universal_newlines=True)
        # Parse the echo output.
        new_env = {}
        for line in echo_output.split('\n'):
            split_line = line.split('=', 1)
            if len(split_line) < 2:
                continue
            new_env[split_line[0]] = split_line[1]

        solution_file_name = self._target_name + '.sln'
        subprocess.check_call([run_msbuild_script_path, solution_file_name], cwd=cwd, env=new_env)

    def _build_make(self, cwd):
        subprocess.check_call(['make'], cwd=cwd)

    @property
    def out_dir_prefix(self):
        return self._out_dir_prefix

    @property
    def shared_library_extension(self):
        return self._shared_library_extension

    @property
    def static_library_extension(self):
        return self._static_library_extension

    @property
    def library_prefix(self):
        return self._library_prefix









